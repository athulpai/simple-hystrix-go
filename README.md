# SimpleGoHystrix
Http request similar to hystrix by netflix for golang project

This library offers the following features

- Updating different types of Timeouts
    - Gives a more granular updating of timeouts 
    - update request timeout
    - update response timeout
    - update socket timeout
- Make multiple concurrent calls
- Retires with different types of delays that is available to choose from
- Making a TLS call by just a certificate
